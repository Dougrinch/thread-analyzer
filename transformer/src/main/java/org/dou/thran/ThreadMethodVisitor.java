package org.dou.thran;

import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Type.*;

public class ThreadMethodVisitor extends MethodVisitor {

    public ThreadMethodVisitor(MethodVisitor mv) {
        super(ASM5, mv);
    }

    @Override
    public void visitCode() {
        super.visitVarInsn(ALOAD, 0);
        super.visitMethodInsn(INVOKESTATIC, "org/dou/thran/ThreadAnalyzer", "beforeStart", getMethodDescriptor(VOID_TYPE, getType(Thread.class)), false);
        super.visitCode();
    }
}
