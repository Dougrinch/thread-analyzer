package org.dou.thran;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import static org.objectweb.asm.ClassWriter.COMPUTE_MAXS;

public class ThreadTransformer implements ClassFileTransformer {

    private final String threadClassName = Type.getInternalName(Thread.class);

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        if (!className.equals(threadClassName))
            return classfileBuffer;

        return transform(new ClassReader(classfileBuffer));
    }

    private byte[] transform(ClassReader reader) {
        ClassWriter writer = new ClassWriter(reader, COMPUTE_MAXS);
        ThreadClassVisitor visitor = new ThreadClassVisitor(writer);
        reader.accept(visitor, 0);
        return writer.toByteArray();
    }
}
