package org.dou.thran;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.ASM5;

public class ThreadClassVisitor extends ClassVisitor {

    public ThreadClassVisitor(ClassVisitor classVisitor) {
        super(ASM5, classVisitor);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
        if (mv == null)
            return null;

        if (!"start".equals(name))
            return mv;

        return new ThreadMethodVisitor(mv);
    }
}
