package org.dou.thran;

import java.io.PrintStream;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class ThreadAnalyzer {

    private static final Set<ThreadMetadata> threads = new CopyOnWriteArraySet<ThreadMetadata>();

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(new ShutdownHook()));
    }

    public static void beforeStart(Thread thread) {
        if (!threads.add(new ThreadMetadata(thread)))
            throw new IllegalStateException();
    }

    private static class ShutdownHook implements Runnable {

        @Override
        public void run() {
            for (ThreadMetadata metadata : threads) {
                if (!isInterestThread(metadata))
                    continue;

                metadata.printStartPoint(System.err);
                System.err.println("---------------------------------");
            }
        }

        private boolean isInterestThread(ThreadMetadata metadata) {
            Thread thread = metadata.getThread();

            if (thread.equals(Thread.currentThread()))
                return false;

            if (!thread.isAlive())
                return false;

            if (thread.isDaemon())
                return false;

            StackTraceElement topFrame = metadata.getTopFrame();
            if (topFrame.getClassName().equals("java.lang.ApplicationShutdownHooks")
                    && topFrame.getMethodName().equals("runHooks"))
                return false;

            return true;
        }
    }

    private static class ThreadMetadata {

        private final Thread thread;
        private final StackTraceElement[] startPoint;

        public ThreadMetadata(Thread thread) {
            this.thread = thread;

            StackTraceElement[] orig = new Exception().getStackTrace();
            startPoint = new StackTraceElement[orig.length - 3];
            System.arraycopy(orig, 3, startPoint, 0, startPoint.length);
        }

        public Thread getThread() {
            return thread;
        }

        public void printStartPoint(PrintStream stream) {
            Exception startedPoint = new Exception(thread.toString());
            startedPoint.setStackTrace(startPoint);
            startedPoint.printStackTrace(stream);
        }

        public StackTraceElement getTopFrame() {
            return startPoint[0];
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ThreadMetadata that = (ThreadMetadata) o;

            return thread.equals(that.thread);
        }

        @Override
        public int hashCode() {
            return thread.hashCode();
        }
    }
}
