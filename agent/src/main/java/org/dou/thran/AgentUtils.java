package org.dou.thran;

import com.devexperts.aprof.util.FastByteBuffer;

import java.io.IOException;
import java.io.InputStream;
import java.lang.instrument.*;
import java.net.URL;
import java.util.*;
import java.util.jar.Manifest;

public class AgentUtils {

    public static Manifest findManifest(Class<?> premainClazz) throws IOException {
        Enumeration<URL> resources = Thread.currentThread().getContextClassLoader().getResources("META-INF/MANIFEST.MF");
        while (resources.hasMoreElements()) {
            try (InputStream is = resources.nextElement().openStream()) {
                Manifest manifest = new Manifest(is);

                String premainClass = manifest.getMainAttributes().getValue("Premain-Class");

                if (!premainClazz.getName().equals(premainClass))
                    continue;

                return manifest;
            }
        }

        throw new RuntimeException("manifest not found");
    }

    public static void redefine(Instrumentation inst, ClassFileTransformer transformer)
            throws IllegalClassFormatException, UnmodifiableClassException, ClassNotFoundException {
        ArrayList<Class> classes = new ArrayList<>();
        HashSet<Class> done = new HashSet<>();
        FastByteBuffer buf = new FastByteBuffer();
        while (true) {
            classes.addAll(Arrays.asList(inst.getAllLoadedClasses()));
            List<ClassDefinition> cdl = new ArrayList<>(classes.size());
            for (Class clazz : classes) {
                if (clazz.isArray())
                    continue;
                if (!done.add(clazz))
                    continue;
                String name = clazz.getName().replace('.', '/');
                InputStream is = clazz.getResourceAsStream("/" + name + ".class");
                buf.clear();
                if (is != null)
                    try {
                        try {
                            buf.readFrom(is);
                        } finally {
                            is.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                if (buf.isEmpty()) {
                    continue;
                }
                byte[] result = transformer.transform(clazz.getClassLoader(), name, clazz, clazz.getProtectionDomain(), buf.getBytes());
                if (result != null)
                    cdl.add(new ClassDefinition(clazz, result));
            }
            classes.clear();
            if (cdl.isEmpty())
                break;

            inst.redefineClasses(cdl.toArray(new ClassDefinition[cdl.size()]));
        }
    }
}
