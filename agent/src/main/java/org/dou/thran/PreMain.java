package org.dou.thran;

import com.devexperts.aprof.util.InnerJarClassLoader;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.jar.Manifest;

import static org.dou.thran.AgentUtils.findManifest;
import static org.dou.thran.AgentUtils.redefine;

public class PreMain {

    public static void premain(String agentArgs, Instrumentation inst) throws Exception {
        Manifest manifest = findManifest(PreMain.class);

        List<URL> urls = new ArrayList<>();

        for (String jar : Arrays.asList(manifest.getMainAttributes().getValue("Transformer-Jars").split(" "))) {
            URL url = Thread.currentThread().getContextClassLoader().getResource(jar);
            if (url != null)
                urls.add(url);
        }

        InnerJarClassLoader innerJarClassLoader = new InnerJarClassLoader(urls);
        Class<?> transformerClass = innerJarClassLoader.loadClass(manifest.getMainAttributes().getValue("Transformer-Class"));
        ClassFileTransformer transformer = (ClassFileTransformer) transformerClass.newInstance();

        redefine(inst, transformer);

        inst.addTransformer(transformer);
    }
}
