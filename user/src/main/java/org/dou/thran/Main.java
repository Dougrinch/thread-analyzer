package org.dou.thran;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws IOException {
        ExecutorService leakyExecutor = Executors.newSingleThreadExecutor();

        leakyExecutor.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("Leaky thread was started.");
            }
        });

        System.out.println("wait stop signal...");

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while ((line = br.readLine()) != null) {
                if ("stop".equals(line))
                    break;
            }
        }

        System.out.println("Complete stopped");
    }
}
